import java.util.*;
class Array27{
        public static void main(String [] args){
                Scanner sc=new Scanner (System.in);
                System.out.print("Enter the size ");
                int size=sc.nextInt();
                int arr[]=new int[size];

                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
		int min=arr[0];
		for(int i=1;i<size;i++){
			if(arr[i]<min){
				min=arr[i];
			}
		}
		int min2=arr[0];
		for(int i=0;i<size;i++){
			if(arr[i]<=min2 &&arr[i]>min){
				min2=arr[i];
			}
		}
		System.out.println("The 2 nd minimum element in the array is  "+min2);
	}
}

