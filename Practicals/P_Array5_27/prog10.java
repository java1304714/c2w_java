import java.util.*;
class Array27{
        public static void main(String [] args){
                Scanner sc=new Scanner (System.in);
                System.out.print("Enter the size ");
                int size=sc.nextInt();
                int arr[]=new int[size+1];

                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
		for(int i=0;i<size;i++){
			int fact=1;
			while(arr[i]>=1){
				fact=arr[i]*fact;
				arr[i]--;
			}
			System.out.print(fact +" ");
		}
	}
}
