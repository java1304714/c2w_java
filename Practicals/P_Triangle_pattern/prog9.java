import java.io.*;
class Triangle{
        public static void main(String[] args )throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter the row : ");
                int row=Integer.parseInt(br.readLine());
                for (int i=1 ;i<=row;i++){
                        int num=64+row;
                        for(int j=1;j<=row-i+1;j++){

                                System.out.print((char)num-- +" ");
                        }
                        System.out.println();

                }
        }
}
