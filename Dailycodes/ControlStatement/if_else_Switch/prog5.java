// Switch Statement

class Switch{
        public static void main(String [] Args){
                int num=2;
                System.out.println("Before");
                switch(num){
                        case 1:
                                System.out.println("one");
				break;

                        case 2:
                                System.out.println("two");
				break;
                        case 3:
                                System.out.println("Three");
                                break;
                }
                System.out.println("After");
        }
}
