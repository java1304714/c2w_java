// Switch Statement

class Switch{
        public static void main(String [] Args){
                char data='b';
                System.out.println("Before");
                switch(data){
                        case 'a':
                                System.out.println("one");
                                break;

                        case 2:
                                System.out.println("two");
                                break;
                        case 'b':
                                System.out.println("Three");
                                break;
                        default:
                                System.out.println("BY Default");
                }
                System.out.println("After");
        }
}
