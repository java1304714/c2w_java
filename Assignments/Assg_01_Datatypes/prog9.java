/*Write a program to print the temperature of the Air Conditioner in
degrees and also print the standard room temperature using appropriate
data types.*/

class Temperature{
	public static void main(String[] args){

		double ACTemp=25.5;
		float RoomTemp=35f;

		System.out.println("Temp of AC is :"+ ACTemp + " degrees Celsius");
		System.out.println("Room temp is:"+ RoomTemp + " degrees Celsius");
	}
}
