import java.util.*;
class TPattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the row: ");
		int row=sc.nextInt();
		
		for(int i=1;i<=row;i++){
		        char ch1=65;
                        char ch2=97;
			for(int j=1;j<=row-i+1;j++){
				if(i%2==0){
					System.out.print(ch2 +" ");
				}else{
					System.out.print(ch1 +" ");
				}
				ch1++;
				ch2++;
			}
			System.out.println();
		}
	}
}
