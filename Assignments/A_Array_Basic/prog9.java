import java.util.*;
class Array{
        public static void main(String [] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter the array Size :");
                int size=sc.nextInt();
                int arr[]=new int[size];
                for(int i=0;i<arr.length;i++){
                        System.out.print("Enter element : ");
                        arr[i]=sc.nextInt();
                }
                System.out.println("Elements in array : ");
                for(int i=0;i<arr.length;i++){
                        if(i%2==1){
                                System.out.println(arr[i] +" Is an odd indexed element");
                        }
                }
        }
}
